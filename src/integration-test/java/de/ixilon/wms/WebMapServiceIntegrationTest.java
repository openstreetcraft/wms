// Copyright (C) 2016 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.wms;

import static org.junit.Assert.assertEquals;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.InvalidMediaTypeException;
import org.springframework.http.MediaType;

import de.ixilon.wms.exception.LayerNotDefinedException;
import de.ixilon.wms.exception.MissingParameterValueException;
import de.ixilon.wms.exception.ServiceException;

public class WebMapServiceIntegrationTest {

  private WebMapService service;

  @Before
  public void setUp() throws URISyntaxException {
    service = new WebMapService(new URI("http://winsent-geoserver:8080/geoserver/wms"));
  }

  @Test
  public void getsCapabilitiesVersion() throws ServiceException {
    assertEquals("1.3.0", service.getCapabilities(new MetadataRequest()).getVersion());
  }

  @Test(expected = InvalidMediaTypeException.class)
  public void throwsInvalidMediaTypeException() throws ServiceException {
    service.getMap(new MapRequest());
  }

  @Test(expected = MissingParameterValueException.class)
  public void throwsMissingParameterValueException() throws ServiceException {
    service.getMap(new MapRequest().setFormat("image/png"));
  }

  @Test(expected = LayerNotDefinedException.class)
  public void throwsLayerNotDefinedException() throws ServiceException {
    service.getMap(new MapRequest().setFormat("image/png").setWidth(16).setHeight(16));
  }

  @Test
  public void getsMap() throws ServiceException {
    MapRequest request = new MapRequest().setFormat("image/png").setWidth(16).setHeight(16)
        .setBbox(0, 0, 15, 15).setLayers("tasmania");

    MediaResponse response = service.getMap(request);
    MediaType expected = MediaType.IMAGE_PNG;
    MediaType actual = response.getContentType();
    assertEquals(expected, actual);
  }

  @Test
  public void getsFeatureInfo() throws ServiceException {
    MapRequest mapRequest = new MapRequest().setFormat("image/png").setWidth(16).setHeight(16)
        .setBbox(0, 0, 15, 15).setLayers("tasmania");

    FeatureInfoRequest featureInfoRequest = new FeatureInfoRequest(mapRequest)
        .setFormat("application/json").setLayers("tasmania").setPointOfInterest(0, 0);

    MediaResponse response = service.getFeatureInfo(featureInfoRequest);
    MediaType expected = MediaType.APPLICATION_JSON_UTF8;
    MediaType actual = response.getContentType();
    assertEquals(expected, actual);
  }

}

// Copyright (C) 2016 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.wms;

import static org.junit.Assert.assertEquals;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;

public class MetadataRequestTest {

  private URI uri;
  private String url;

  @Before
  public void setUp() throws URISyntaxException {
    uri = new URI("http://server");
    url = uri.toString() + "?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetCapabilities";
  }

  @Test
  public void buildsGetCapabilitiesRequest() throws URISyntaxException {
    MetadataRequest request = new MetadataRequest();
    URI expected = new URI(url);
    URI actual = request.build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsGetCapabilitiesRequestWithSequence() throws URISyntaxException {
    MetadataRequest request = new MetadataRequest().setSequence(42);
    URI expected = new URI(url + "&UPDATESEQUENCE=42");
    URI actual = request.build(uri);
    assertEquals(expected, actual);
  }

}

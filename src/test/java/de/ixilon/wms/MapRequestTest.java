// Copyright (C) 2016 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.wms;

import static org.junit.Assert.assertEquals;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import de.ixilon.wms.MapRequest.Dimension;

public class MapRequestTest {

  private MapRequest request;
  private URI uri;
  private String url;

  @Before
  public void setUp() throws URISyntaxException {
    request = new MapRequest();
    uri = new URI("http://server");
    url = uri.toString() + "?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap";
  }

  @Test
  public void buildsMapRequest() throws URISyntaxException {
    URI expected = new URI(url);
    URI actual = request.build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsMapRequestWithBoundingBox() throws URISyntaxException {
    URI expected = new URI(url + "&BBOX=1.0,2.2,3.33,4.444");
    URI actual = request.setBbox(1, 2.2, 3.33, 4.444).build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsMapRequestWithCoordinateReferenceSystem() throws URISyntaxException {
    URI expected = new URI(url + "&CRS=foo:bar");
    URI actual = request.setCrs("foo:bar").build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsMapRequestWithSingleLayer() throws URISyntaxException {
    URI expected = new URI(url + "&LAYERS=foo");
    URI actual = request.setLayers("foo").build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsMapRequestWithVariadicLayers() throws URISyntaxException {
    URI expected = new URI(url + "&LAYERS=foo,bar");
    URI actual = request.setLayers("foo", "bar").build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsMapRequestWithLayersList() throws URISyntaxException {
    URI expected = new URI(url + "&LAYERS=foo,bar");
    URI actual = request.setLayers(Arrays.asList("foo", "bar")).build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsMapRequestWithSingleStyle() throws URISyntaxException {
    URI expected = new URI(url + "&STYLES=foo");
    URI actual = request.setStyles("foo").build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsMapRequestWithVariadicStyles() throws URISyntaxException {
    URI expected = new URI(url + "&STYLES=foo,bar");
    URI actual = request.setStyles("foo", "bar").build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsMapRequestWithStylesList() throws URISyntaxException {
    URI expected = new URI(url + "&STYLES=foo,bar");
    URI actual = request.setStyles(Arrays.asList("foo", "bar")).build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsMapRequestWithBackgroundColor() throws URISyntaxException {
    MapRequest request = new MapRequest().setBgcolor(1, 2, 3);
    URI expected = new URI(url + "&BGCOLOR=0x010203");
    URI actual = request.build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsMapRequestWithFormat() throws URISyntaxException {
    URI expected = new URI(url + "&FORMAT=image/png");
    URI actual = request.setFormat("image/png").build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsMapRequestWithWidth() throws URISyntaxException {
    URI expected = new URI(url + "&WIDTH=42");
    URI actual = request.setWidth(42).build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsMapRequestWithHeight() throws URISyntaxException {
    URI expected = new URI(url + "&HEIGHT=42");
    URI actual = request.setHeight(42).build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsMapRequestWithTransparence() throws URISyntaxException {
    URI expected = new URI(url + "&TRANSPARENT=TRUE");
    URI actual = request.setTransparent(true).build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsMapRequestWithIntransparence() throws URISyntaxException {
    URI expected = new URI(url + "&TRANSPARENT=FALSE");
    URI actual = request.setTransparent(false).build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsMapRequestWithTime() throws URISyntaxException {
    URI expected = new URI(url + "&TIME=foo");
    URI actual = request.setTime(new Dimension("foo")).build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsMapRequestWithElevation() throws URISyntaxException {
    URI expected = new URI(url + "&ELEVATION=foo");
    URI actual = request.setElevation(new Dimension("foo")).build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsMapRequestWithDimension() throws URISyntaxException {
    URI expected = new URI(url + "&DIM_foo=bar");
    URI actual = request.addDimension("foo", new Dimension("bar")).build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void formatsDimensionValue() {
    String expected = "foo";
    String actual = new Dimension("foo").toString();
    assertEquals(expected, actual);
  }

  @Test
  public void formatsDimensionValues() {
    String expected = "foo,bar";
    String actual = new Dimension(Arrays.asList("foo", "bar")).toString();
    assertEquals(expected, actual);
  }

  @Test
  public void formatsDimensionInterval() {
    String expected = "min/max/resolution";
    String actual = new Dimension("min", "max", "resolution").toString();
    assertEquals(expected, actual);
  }

  @Test
  public void formatsDimensionIntervals() {
    Dimension[] dimensions = {new Dimension("min1", "max1", "resolution1"),
        new Dimension("min2", "max2", "resolution2")};
    String expected = "min1/max1/resolution1,min2/max2/resolution2";
    String actual = new Dimension(dimensions).toString();
    assertEquals(expected, actual);
  }

}

// Copyright (C) 2016 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.wms;

import static org.junit.Assert.assertEquals;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

public class FeatureInfoRequestTest {

  private FeatureInfoRequest request;
  private URI uri;
  private String url;

  @Before
  public void setUp() throws URISyntaxException {
    request = new FeatureInfoRequest(new MapRequest());
    uri = new URI("http://server");
    url = uri.toString() + "?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetFeatureInfo";
  }

  @Test
  public void buildsFeatureInfoRequest() throws URISyntaxException {
    URI expected = new URI(url);
    URI actual = request.build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsFeatureInfoRequestWithSingleQueryLayer() throws URISyntaxException {
    URI expected = new URI(url + "&QUERY_LAYERS=foo");
    URI actual = request.setLayers("foo").build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsFeatureInfoRequestWithVariadicQueryLayers() throws URISyntaxException {
    URI expected = new URI(url + "&QUERY_LAYERS=foo,bar");
    URI actual = request.setLayers("foo", "bar").build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsFeatureInfoRequestWithQueryLayersList() throws URISyntaxException {
    URI expected = new URI(url + "&QUERY_LAYERS=foo,bar");
    URI actual = request.setLayers(Arrays.asList("foo", "bar")).build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsFeatureInfoRequestWithInfoFormat() throws URISyntaxException {
    URI expected = new URI(url + "&INFO_FORMAT=foo");
    URI actual = request.setFormat("foo").build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsFeatureInfoRequestWithFeatureCount() throws URISyntaxException {
    URI expected = new URI(url + "&FEATURE_COUNT=42");
    URI actual = request.setFeatureCount(42).build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsFeatureInfoRequestWithPointOfInterest() throws URISyntaxException {
    URI expected = new URI(url + "&I=1&J=2");
    URI actual = request.setPointOfInterest(1, 2).build(uri);
    assertEquals(expected, actual);
  }

  @Test
  public void buildsFeatureInfoRequestWithMapRequestPart() throws URISyntaxException {
    request = new FeatureInfoRequest(new MapRequest().setFormat("foo"));
    URI expected = new URI(uri.toString()
        + "?FORMAT=foo&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetFeatureInfo&INFO_FORMAT=bar");
    URI actual = request.setFormat("bar").build(uri);
    assertEquals(expected, actual);
  }

}

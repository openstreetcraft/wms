// Copyright (C) 2016 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.wms;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.util.StreamUtils;

/**
 * Wrapper for a {@link HttpInputMessage} that buffers all streams in memory.
 *
 * <p>
 * Using this wrapper allows for multiple reads of the
 * {@link HttpInputMessage#getBody()} response body.
 */
class BufferingHttpInputMessage implements HttpInputMessage {

  private final HttpInputMessage inputMessage;
  private byte[] buffer;

  public BufferingHttpInputMessage(HttpInputMessage inputMessage) {
    this.inputMessage = inputMessage;
  }

  @Override
  public HttpHeaders getHeaders() {
    return inputMessage.getHeaders();
  }

  @Override
  public synchronized InputStream getBody() throws IOException {
    if (buffer == null) {
      buffer = StreamUtils.copyToByteArray(inputMessage.getBody());
    }
    return new ByteArrayInputStream(buffer);
  }
  
}

// Copyright (C) 2016 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.wms;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import de.ixilon.wms.exception.ServiceException;
import net.opengis.wms.WMSCapabilities;

/**
 * A Web Map Service (WMS) produces maps of spatially referenced data dynamically from geographic
 * information. This International Standard defines a map to be a portrayal of geographic
 * information as a digital image file suitable for display on a computer screen. A map is not the
 * data itself. WMS-produced maps are generally rendered in a pictorial format such as PNG, GIF or
 * JPEG, or occasionally as vector-based graphical elements in Scalable Vector Graphics (SVG) or Web
 * Computer Graphics Metafile (WebCGM) formats.
 * 
 * <p>
 * This International Standard defines three operations: one returns service-level metadata; another
 * returns a map whose geographic and dimensional parameters are well-defined; and an optional third
 * operation returns information about particular features shown on a map. Web Map Service
 * operations can be invoked using a standard web browser by submitting requests in the form of
 * Uniform Resource Locators (URLs). The content of such URLs depends on which operation is
 * requested. In particular, when requesting a map the URL indicates what information is to be shown
 * on the map, what portion of the Earth is to be mapped, the desired coordinate reference system,
 * and the output image width and height. When two or more maps are produced with the same
 * geographic parameters and output size, the results can be accurately overlaid to produce a
 * composite map. The use of image formats that support transparent backgrounds (e.g. GIF or PNG)
 * allows underlying maps to be visible. Furthermore, individual maps can be requested from
 * different servers. The Web Map Service thus enables the creation of a network of distributed map
 * servers from which clients can build customized maps.
 * 
 * <p>
 * This International Standard applies to a Web Map Service instance that publishes its ability to
 * produce maps rather than its ability to access specific data holdings. A basic WMS classifies its
 * geographic information holdings into Layers and offers a finite number of predefined Styles
 * in which to display those layers. This International Standard supports only named Layers and
 * Styles, and does not include a mechanism for user-defined symbolization of feature data.
 * 
 * <p>
 * http://www.opengeospatial.org/standards/wms
 */
public class WebMapService {

  private final RestTemplate restTemplate;
  private final URI url;
  private final List<HttpMessageConverter<?>> defaultMessageConverters;

  public WebMapService(URI url) {
    this(url, new RestTemplate());
  }

  /**
   * Create a Web Map Service client instance connected to the given endpoint. 
   * @param url WMS server endpoint http://server:port/path
   * @param restTemplate optional Spring REST template to use
   */
  public WebMapService(URI url, RestTemplate restTemplate) {
    this.url = url;
    this.restTemplate = restTemplate;
    this.defaultMessageConverters = new ArrayList<>(restTemplate.getMessageConverters());
  }

  /**
   * The purpose of the mandatory GetCapabilities operation is to obtain service metadata, which is
   * a machine- readable (and human-readable) description of the servers information content and
   * acceptable request parameter values.
   */
  public WMSCapabilities getCapabilities(MetadataRequest request) throws ServiceException {
    addMessageConverter(new MetadataResponseConverter());
    return doRequest(request, WMSCapabilities.class);
  }

  /**
   * GetFeatureInfo is an optional operation. It is only supported for those Layers for which the
   * attribute queryable="1" (true) has been defined or inherited. A client shall not issue a
   * GetFeatureInfo request for other layers. A WMS shall respond with a properly formatted service
   * exception (XML) response (code = OperationNotSupported) if it receives a GetFeatureInfo request
   * but does not support it.
   * 
   * <p>
   * The GetFeatureInfo operation is designed to provide clients of a WMS with more information
   * about features in the pictures of maps that were returned by previous Map requests. The
   * canonical use case for GetFeatureInfo is that a user sees the response of a Map request and
   * chooses a point (I,J) on that map for which to obtain more information. The basic operation
   * provides the ability for a client to specify which pixel is being asked about, which layer(s)
   * should be investigated, and what format the information should be returned in. Because the WMS
   * protocol is stateless, the GetFeatureInfo request indicates to the WMS what map the user is
   * viewing by including most of the original GetMap request parameters (all but VERSION and
   * REQUEST). From the spatial context information (BBOX, CRS, WIDTH, HEIGHT) in that GetMap
   * request, along with the I,J position the user chose, the WMS can (possibly) return additional
   * information about that position.
   * 
   * <p>
   * The actual semantics of how a WMS decides what to return more information about, or what
   * exactly to return, are left up to the WMS provider.
   */
  public MediaResponse getFeatureInfo(FeatureInfoRequest request) throws ServiceException {
    addMessageConverter(new MediaResponseConverter(request));
    return doRequest(request, MediaResponse.class);
  }

  /**
   * The GetMap operation returns a map. Upon receiving a GetMap request, a WMS shall either satisfy
   * the request or issue a service exception.
   */
  public MediaResponse getMap(MapRequest request) throws ServiceException {
    addMessageConverter(new MediaResponseConverter(request));
    return doRequest(request, MediaResponse.class);
  }

  private void addMessageConverter(HttpMessageConverter<?> messageConverter) {
    List<HttpMessageConverter<?>> newMessageConverters = new ArrayList<>(defaultMessageConverters);
    newMessageConverters.add(0, messageConverter);
    restTemplate.setMessageConverters(newMessageConverters);
  }

  private <T> T doRequest(ServiceRequest request, Class<T> responseType) throws ServiceException {
    try {
      return restTemplate.getForEntity(request.build(url), responseType).getBody();
    } catch (RestClientException exception) {
      ServiceExceptionParser.checkException(exception);
      throw exception;
    }
  }

}

// Copyright (C) 2016 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.wms;

import java.io.IOException;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.client.RestClientException;

import de.ixilon.wms.exception.ServiceException;
import net.opengis.ogc.ServiceExceptionReport;
import net.opengis.ogc.ServiceExceptionType;

class ServiceExceptionParser {

  private static final JaxbConverter<ServiceExceptionReport> converter =
      new JaxbConverter<>(ServiceExceptionReport.class);

  public static void checkException(HttpInputMessage inputMessage) throws IOException {
    final ServiceExceptionReport report;

    try {
      report = converter.readInternal(ServiceExceptionReport.class, inputMessage);
    } catch (HttpMessageNotReadableException exception) {
      return;
    }

    for (ServiceExceptionType type : report.getServiceException()) {
      throw ServiceException.of(type);
    }
  }

  public static void checkException(RestClientException exception) throws ServiceException {
    Throwable cause = exception.getCause();
    if (cause instanceof ServiceException) {
      throw (ServiceException) exception.getCause();
    }
  }

}

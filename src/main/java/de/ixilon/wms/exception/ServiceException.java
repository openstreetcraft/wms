// Copyright (C) 2016 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.wms.exception;

import java.io.IOException;

import net.opengis.ogc.ServiceExceptionType;

public class ServiceException extends IOException {

  private static final long serialVersionUID = 1L;
  private final ServiceExceptionType serviceExceptionType;

  protected ServiceException(ServiceExceptionType serviceExceptionType) {
    super(serviceExceptionType.getValue());
    this.serviceExceptionType = serviceExceptionType;
  }

  /**
   * Abstract factory creating an exception object.
   */
  public static ServiceException of(ServiceExceptionType serviceExceptionType) {
    String code = serviceExceptionType.getCode();
    if (code != null) {
      switch (code) {
        case "InvalidFormat":
          return new InvalidFormatException(serviceExceptionType);
        case "InvalidCRS":
          return new InvalidCoordinateReferenceSystemException(serviceExceptionType);
        case "LayerNotDefined":
          return new LayerNotDefinedException(serviceExceptionType);
        case "StyleNotDefined":
          return new StyleNotDefinedException(serviceExceptionType);
        case "LayerNotQueryable":
          return new LayerNotQueryableException(serviceExceptionType);
        case "InvalidPoint":
          return new InvalidPointException(serviceExceptionType);
        case "CurrentUpdateSequence":
          return new CurrentUpdateSequenceException(serviceExceptionType);
        case "InvalidUpdateSequence":
          return new InvalidUpdateSequenceException(serviceExceptionType);
        case "MissingBBox":
        case "MissingDimensionValue":
        case "MissingOrInvalidParameter":
        case "MissingParameterValue":
          return new MissingParameterValueException(serviceExceptionType);
        case "InvalidDimensionValue":
          return new InvalidDimensionValueException(serviceExceptionType);
        case "OperationNotSupported":
          return new OperationNotSupportedException(serviceExceptionType);
        default:
          break;
      }
    }
    return new ServiceException(serviceExceptionType);
  }

  public String getCode() {
    return serviceExceptionType.getCode();
  }

  public String getLocator() {
    return serviceExceptionType.getLocator();
  }

}

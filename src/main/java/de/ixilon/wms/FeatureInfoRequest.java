// Copyright (C) 2016 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.wms;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.common.base.Joiner;

public class FeatureInfoRequest extends ServiceRequest implements MediaRequest {

  private final MapRequest mapRequest;

  private String layers;
  private String format;
  private String count;
  private String column;
  private String row;

  public FeatureInfoRequest(MapRequest mapRequest) {
    this.mapRequest = mapRequest;
  }

  @Override
  protected URI build(URI url) {
    UriComponentsBuilder builder = UriComponentsBuilder.fromUri(mapRequest.build(url));

    builder.replaceQueryParam("SERVICE", "WMS");
    builder.replaceQueryParam("VERSION", "1.3.0");
    builder.replaceQueryParam("REQUEST", "GetFeatureInfo");

    addQueryParam(builder, "QUERY_LAYERS", layers);
    addQueryParam(builder, "INFO_FORMAT", format);
    addQueryParam(builder, "FEATURE_COUNT", count);
    addQueryParam(builder, "I", column);
    addQueryParam(builder, "J", row);

    return builder.build().encode().toUri();
  }

  @Override
  public MediaType getMediaType() {
    return MediaType.parseMediaType(format);
  }

  /**
   * The mandatory QUERY_LAYERS parameter states the map layer(s) from which feature information is
   * desired to be retrieved. Its value is a comma-separated list of one or more map layers. This
   * parameter shall contain at least one layer name, but may contain fewer layers than the original
   * GetMap request.
   * 
   * <p>
   * If any layer in the QUERY_LAYERS parameter is not defined in the service metadata of the WMS,
   * the server shall issue a service exception (code = LayerNotDefined).
   */
  public FeatureInfoRequest setLayers(String... layers) {
    return setLayers(Arrays.asList(layers));
  }

  /**
   * @see #setLayers(String...)
   */
  public FeatureInfoRequest setLayers(List<String> layers) {
    this.layers = Joiner.on(",").join(layers);
    return this;
  }

  /**
   * The mandatory INFO_FORMAT parameter indicates what format to use when returning the feature
   * information. Supported values for a GetFeatureInfo request on a WMS server are listed as MIME
   * types in one or more Request/FeatureInfo/Format elements of its service metadata. The entire
   * MIME type string is used as the value of the INFO_FORMAT parameter. In an HTTP environment, the
   * MIME type shall be set on the returned object using the Content-type entity header. If the
   * request specifies a format not supported by the server, the server shall issue a service
   * exception (code = InvalidFormat).
   * 
   * <p>
   * The parameter INFO_FORMAT=text/xml requests that the feature information be formatted in XML.
   */
  public FeatureInfoRequest setFormat(String format) {
    this.format = format;
    return this;
  }

  /**
   * The optional FEATURE_COUNT parameter states the maximum number of features per layer for which
   * feature information shall be returned. Its value is a positive integer. The default value is 1
   * if this parameter is omitted or is other than a positive integer.
   */
  public FeatureInfoRequest setFeatureCount(int count) {
    this.count = Integer.toString(count);
    return this;
  }

  /**
   * The mandatory I (column) and J (row) request parameters are integers that indicate a point of
   * interest on the map that was produced by the embedded GetMap request (the map request part).
   * The point (I,J) is a point in the (i,j) space defined by the Map CS. Therefore:
   * <ul>
   * <li>the value of I shall be between 0 and the maximum value of the i axis
   * <li>the value of J shall be between 0 and the maximum value of the j axis
   * <li>the point I=0, J=0 indicates the pixel at the upper left corner of the map
   * <li>I increases to the right and J increases downward
   * </ul>
   * The point (I,J) represents the centre of the indicated pixel. If the value of I or of J is
   * invalid, the server shall issue a service exception (code = InvalidPoint).
   */
  public FeatureInfoRequest setPointOfInterest(int column, int row) {
    this.column = Integer.toString(column);
    this.row = Integer.toString(row);
    return this;
  }

}

// Copyright (C) 2016 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.wms;

import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

public class JaxbConverter<T> extends AbstractHttpMessageConverter<T> {

  private final Class<T> clazz;
  private final Unmarshaller unmarshaller;
  private final Marshaller marshaller;

  /**
   * Converter for text/xml media types.
   */
  public JaxbConverter(Class<T> clazz) {
    super(MediaType.TEXT_XML);
    this.clazz = clazz;
    try {
      JAXBContext context = JAXBContext.newInstance(clazz);
      unmarshaller = context.createUnmarshaller();
      marshaller = context.createMarshaller();
    } catch (JAXBException exception) {
      throw new IllegalArgumentException(clazz.getName(), exception);
    }
  }

  @Override
  protected boolean supports(Class<?> clazz) {
    return clazz.isAssignableFrom(this.clazz);
  }

  @Override
  protected T readInternal(Class<? extends T> clazz, HttpInputMessage inputMessage)
      throws IOException, HttpMessageNotReadableException {
    try {
      return (T) unmarshaller.unmarshal(inputMessage.getBody());
    } catch (JAXBException exception) {
      throw new HttpMessageNotReadableException("unmarshall error", exception);
    }
  }

  @Override
  protected void writeInternal(T obj, HttpOutputMessage outputMessage)
      throws IOException, HttpMessageNotWritableException {
    try {
      marshaller.marshal(obj, outputMessage.getBody());
    } catch (JAXBException exception) {
      throw new HttpMessageNotWritableException("marshall error", exception);
    }
  }

}

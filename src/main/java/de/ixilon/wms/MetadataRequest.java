// Copyright (C) 2016 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.wms;

import java.net.URI;

import org.springframework.web.util.UriComponentsBuilder;

public class MetadataRequest extends ServiceRequest {

  private String sequence;

  @Override
  protected URI build(URI url) {
    UriComponentsBuilder builder = UriComponentsBuilder.fromUri(url);

    addQueryParam(builder, "SERVICE", "WMS");
    addQueryParam(builder, "VERSION", "1.3.0");
    addQueryParam(builder, "REQUEST", "GetCapabilities");
    addQueryParam(builder, "UPDATESEQUENCE", sequence);

    return builder.build().encode().toUri();
  }

  /**
   * The optional UPDATESEQUENCE parameter is for maintaining cache consistency.
   * Its value can be either an integer, or a string that represents a timestamp
   * in ISO 8601:2004 format, or any other string. The server may include an
   * UpdateSequence value in its service metadata. If present, this value should
   * be increased when changes are made to the Capabilities (e.g. when new maps
   * are added to the service). The client may include this parameter in its
   * GetCapabilities request. The response of the server based on the presence
   * and relative value of UpdateSequence in the client request and the server
   * metadata shall be according to
   * <table summary="Use of UpdateSequence parameter">
   * <tr>
   * <th>Client request
   * <th>Server metadata
   * <th>Server response
   * <tr>
   * <td>none
   * <td>any
   * <td>most recent service metadata
   * <tr>
   * <td>any
   * <td>none
   * <td>most recent service metadata
   * <tr>
   * <td>equal
   * <td>equal
   * <td>Exception: code=CurrentUpdateSequence
   * <tr>
   * <td>lower
   * <td>higher
   * <td>most recent service metadata
   * <tr>
   * <td>higher
   * <td>lower
   * <td>Exception: code=InvalidUpdateSequence
   * </table>
   */
  public MetadataRequest setSequence(int sequence) {
    this.sequence = Integer.toString(sequence);
    return this;
  }

}

// Copyright (C) 2016 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.wms;

import java.io.IOException;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

class MediaResponseConverter extends AbstractHttpMessageConverter<MediaResponse> {

  public MediaResponseConverter(MediaRequest request) {
    super(request.getMediaType(), MediaType.TEXT_XML);
  }

  @Override
  protected boolean supports(Class<?> clazz) {
    return clazz.isAssignableFrom(MediaResponse.class);
  }

  @Override
  protected MediaResponse readInternal(Class<? extends MediaResponse> clazz,
      HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
    inputMessage = new BufferingHttpInputMessage(inputMessage);
    ServiceExceptionParser.checkException(inputMessage);
    return new MediaResponse(inputMessage);
  }

  @Override
  protected void writeInternal(MediaResponse obj, HttpOutputMessage outputMessage)
      throws IOException, HttpMessageNotWritableException {
    throw new HttpMessageNotWritableException(obj.getClass().getSimpleName() + " not supported");
  }

}

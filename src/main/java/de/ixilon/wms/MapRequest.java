// Copyright (C) 2016 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.wms;

import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.MediaType;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.common.base.Joiner;

public class MapRequest extends ServiceRequest implements MediaRequest {

  private String bbox;
  private String crs;
  private String layers;
  private String styles;
  private String bgcolor;
  private String format;
  private String width;
  private String height;
  private String transparent;
  private String time;
  private String elevation;
  private Map<String, String> dimensions = new HashMap<>();

  @Override
  protected URI build(URI url) {
    UriComponentsBuilder builder = UriComponentsBuilder.fromUri(url);

    addQueryParam(builder, "SERVICE", "WMS");
    addQueryParam(builder, "VERSION", "1.3.0");
    addQueryParam(builder, "REQUEST", "GetMap");
    addQueryParam(builder, "BBOX", bbox);
    addQueryParam(builder, "CRS", crs);
    addQueryParam(builder, "LAYERS", layers);
    addQueryParam(builder, "STYLES", styles);
    addQueryParam(builder, "BGCOLOR", bgcolor);
    addQueryParam(builder, "FORMAT", format);
    addQueryParam(builder, "WIDTH", width);
    addQueryParam(builder, "HEIGHT", height);
    addQueryParam(builder, "TRANSPARENT", transparent);
    addQueryParam(builder, "TIME", time);
    addQueryParam(builder, "ELEVATION", elevation);

    for (String key : dimensions.keySet()) {
      addQueryParam(builder, String.format("DIM_%s", key), dimensions.get(key));
    }

    return builder.build().encode().toUri();
  }

  /**
   * The mandatory BBOX parameter allows a client to request a particular Bounding Box. The value of
   * the BBOX parameter in a GetMap request is a list of comma-separated real numbers in the form
   * "minx,miny,maxx,maxy". These values specify the minimum X, minimum Y, maximum X, and maximum Y
   * values of a region in the Layer CRS of the request. The units, ordering and direction of
   * increment of the x and y axes are as defined by the Layer CRS. The four bounding box values
   * indicate the outside limits of the region. The relation of the Bounding Box to the map pixel
   * matrix is that the bounding box goes around the outside of the pixels of the map rather than
   * through the centres of the map's border pixels. In this context, individual pixels represent an
   * area on the ground.
   * 
   * <p>
   * If a request contains an invalid BBOX (e.g. one whose minimum X is greater than or equal to the
   * maximum X, or whose minimum Y is greater than or equal to the maximum Y) the server shall throw
   * a service exception.
   * 
   * <p>
   * If a request contains a BBOX whose area does not overlap at all with the BoundingBox element in
   * the service metadata for the requested layer, the server shall return empty content (that is, a
   * blank map or an graphic element file with no elements) for that map. Any features that are
   * partly or entirely contained in the Bounding Box shall be returned in the appropriate format.
   * 
   * <p>
   * If the Bounding Box values are not defined for the given CRS (e.g. latitudes greater than 90
   * degrees in CRS:84), the server should return empty content for areas outside the valid range of
   * the CRS.
   * 
   * <p>
   * If the WMS server has declared that a Layer is not subsettable, then the client shall specify
   * exactly the declared Bounding Box values in the GetMap request and the server may issue a
   * service exception otherwise.
   */
  public MapRequest setBbox(double minx, double miny, double maxx, double maxy) {
    this.bbox = String.format("%s,%s,%s,%s", minx, miny, maxx, maxy);
    return this;
  }

  /**
   * The CRS request parameter states what Layer CRS applies to the BBOX request parameter. The
   * value of the CRS parameter in a request to a particular server shall be one of the values
   * defined in that server's service metadata in a CRS element defined or inherited by the
   * requested layer. The same CRS applies to all layers in a single request.
   * 
   * <p>
   * A WMS server is not required to support all possible CRSs, but it shall advertise in its
   * service metadata those CRSs which it does offer and shall accept requests for all advertised
   * CRSs. If a request contains a CRS not offered by a particular server, the server shall throw a
   * service exception (code = InvalidCRS).
   * 
   * <p>
   * Clients are not required to support all possible CRSs. If a client and server do not support
   * any mutually agreeable CRSs, the client may, at its discretion, cease communicating with that
   * server, or search for an intermediary service provider that performs coordinate
   * transformations, or allow the user to choose other disposition methods.
   * 
   * <p>
   * This International Standard does not define a facility for clients to explicitly request
   * reprojection or coordinate transformation. That is, only a single CRS request parameter is
   * defined, so it is not possible to specify a source CRS for selecting geographic information and
   * a different target CRS for the portrayal output. Reprojection may occur implicitly, however, if
   * the server offers multiple CRSs but internally stores its geographic information in a
   * particular CRS.
   * 
   * <p>
   * If the WMS server has declared CRS=CRS:1 for a Layer, then the Layer does not have a
   * well-defined coordinate reference system and should not be shown in conjunction with other
   * layers. The client shall specify CRS=CRS:1 in the GetMap request and the server may issue a
   * service exception otherwise. When this CRS is used in the request, the BBOX parameter units
   * shall be pixels.
   */
  public MapRequest setCrs(String crs) {
    this.crs = crs;
    return this;
  }

  /**
   * The mandatory LAYERS parameter lists the map layer(s) to be returned by this GetMap request.
   * The value of the LAYERS parameter is a comma-separated list of one or more valid layer names.
   * Allowed layer names are the character data content of any Layer/Name element in the service
   * metadata.
   * 
   * <p>
   * A WMS shall render the requested layers by drawing the leftmost in the list bottommost, the
   * next one over that, and so on.
   * 
   * <p>
   * The optional LayerLimit element in the service metadata is a positive integer indicating the
   * maximum number of layers a client is permitted to include in a single GetMap request. If this
   * element is absent, the server imposes no limit.
   */
  public MapRequest setLayers(String... layers) {
    return setLayers(Arrays.asList(layers));
  }

  /**
   * @see #setLayers(String...)
   */
  public MapRequest setLayers(List<String> layers) {
    this.layers = Joiner.on(",").join(layers);
    return this;
  }

  /**
   * The mandatory STYLES parameter lists the style in which each layer is to be rendered. The value
   * of the STYLES parameter is a comma-separated list of one or more valid style names. There is a
   * one-to-one correspondence between the values in the LAYERS parameter and the values in the
   * STYLES parameter. Each map in the list of LAYERS is drawn using the corresponding style in the
   * same position in the list of STYLES. Each style Name shall be one that was defined in a
   * Style/Name element that is either directly contained within, or inherited by, the associated
   * Layer element in service metadata. (In other words, the client may not request a Layer in a
   * Style that was only defined for a different Layer.) A server shall throw a service exception
   * (code = StyleNotDefined) if an unadvertised Style is requested. A client may request the
   * default Style using a null value (as in STYLES=). If several layers are requested with a
   * mixture of named and default styles, the STYLES parameter shall include null values between
   * commas (as in STYLES=style1,,style2,,). If all layers are to be shown using the default
   * style, either the form STYLES= or STYLES=,,, is valid.
   * 
   * <p>
   * If the server advertises several styles for a layer, and the client sends a request for the
   * default style, the choice of which style to use as default is at the discretion of the server.
   * The ordering of styles in the service metadata does not indicate which is the default.
   */
  public MapRequest setStyles(String... styles) {
    return setStyles(Arrays.asList(styles));
  }

  /**
   * @see #setStyles(String...)
   */
  public MapRequest setStyles(List<String> styles) {
    this.styles = Joiner.on(",").join(styles);
    return this;
  }

  /**
   * The optional BGCOLOR parameter is a string that specifies the colour to be used as the
   * background (non-data) pixels of the map. The general format of BGCOLOR is a hexadecimal
   * encoding of an RGB value where two hexadecimal characters are used for each of red, green, and
   * blue colour values. The values can range between 00 and FF (0 and 255, base 10) for each. The
   * format is 0xRRGGBB; either upper or lower case characters are allowed for RR, GG, and BB
   * values. The 0x prefix shall have a lower case x. The default value is 0xFFFFFF
   * (corresponding to the colour white) if this parameter is absent from the request.
   * 
   * <p>
   * When FORMAT is a picture format, a WMS shall set the background pixels to the colour specified
   * by BGCOLOR. When FORMAT is a graphic element format (which does not have an explicit
   * background), or a picture format, a WMS should avoid use of the BGCOLOR value for foreground
   * elements because they would not be visible against a background picture of the same colour.
   * 
   * <p>
   * When the Layer has been declared as opaque (or is an area-filling coverage despite the
   * absence of an opaque declaration), then significant portions, or the entirety, of the map may
   * not show any background at all.
   */
  public MapRequest setBgcolor(int red, int green, int blue) {
    this.bgcolor = String.format("0x%02X%02X%02X", red, green, blue);
    return this;
  }

  /**
   * The mandatory FORMAT parameter states the desired format of the map. Supported values for a
   * GetMap request on a WMS server are listed in one or more Request/GetMap/Format elements of its
   * service metadata. The entire MIME type string in Format is used as the value of the FORMAT
   * parameter. There is no default format. In an HTTP environment, the MIME type shall be set on
   * the returned object using the Content-type entity header. If the request specifies a format not
   * supported by the server, the server shall issue a service exception (code = InvalidFormat).
   */
  public MapRequest setFormat(String format) {
    this.format = format;
    return this;
  }

  /**
   * The mandatory WIDTH and HEIGHT parameters specify the size in integer pixels of the map to be
   * produced. The Map CS applies to the map. WIDTH-1 specifies the maximum value of the i axis in
   * the Map CS, and HEIGHT-1 specifies the maximum value of the j axis in the Map CS.
   * 
   * <p>
   * If the request is for a picture format, the returned picture, regardless of its MIME type,
   * shall have exactly the specified width and height in pixels. In the case where the aspect ratio
   * of the BBOX and the ratio width/height are different, the WMS shall stretch the returned map so
   * that the resulting pixels could themselves be rendered in the aspect ratio of the BBOX. In
   * other words, it shall be possible using this definition to request a map for a device whose
   * output pixels are themselves non-square, or to stretch a map into an image area of a different
   * aspect ratio.
   * 
   * <p>
   * Map distortions will be introduced if the aspect ratio WIDTH/HEIGHT is not commensurate with X,
   * Y and the pixel aspect. Client developers should minimize the possibility that users will
   * inadvertently request or unknowingly receive distorted maps.
   * 
   * <p>
   * If a request is for a graphic element format that does not have explicit width and height, the
   * client shall include the WIDTH and HEIGHT values in the request and a server may use them as
   * helpful information in constructing the output map.
   * 
   * <p>
   * The optional MaxWidth and MaxHeight elements in the service metadata are integers indicating
   * the maximum width and height values that a client is permitted to include in a single GetMap
   * request. If either element is absent, the server imposes no limit on the corresponding
   * parameter.
   * 
   * <p>
   * If the WMS server has declared that a Layer has fixed width and height, then the client shall
   * specify exactly those WIDTH and HEIGHT values in the GetMap request and the server may issue a
   * service exception otherwise.
   */
  public MapRequest setWidth(int width) {
    this.width = Integer.toString(width);
    return this;
  }

  /**
   * Specify the height in integer pixels of the map to be produced.
   * 
   * <p>
   * See {@link #setWidth(int)}
   */
  public MapRequest setHeight(int height) {
    this.height = Integer.toString(height);
    return this;
  }

  /**
   * The optional TRANSPARENT parameter specifies whether the map background is to be made
   * transparent or not. TRANSPARENT can take on two values, "TRUE" or "FALSE". The default value is
   * FALSE if this parameter is absent from the request.
   * 
   * <p>
   * The ability to return pictures drawn with transparent pixels allows results of different Map
   * requests to be overlaid, producing a composite map. It is strongly recommended that every WMS
   * offer a format that provides transparency for layers that could sensibly be overlaid above
   * others.
   * 
   * <p>
   * The image/gif format provides transparency and is properly displayed by common web clients. The
   * image/png format provides a range of transparency options but support in viewing applications
   * is less common. The image/jpeg format does not provide transparency at all.
   * 
   * <p>
   * When TRANSPARENT is set to TRUE and the FORMAT parameter contains a Picture format (e.g.
   * image/gif), then a WMS shall return (when permitted by the requested format) a result where all
   * of the pixels not representing features or data values in that Layer are set to a transparent
   * value. For example, a roads layer would be transparent wherever no road is shown. If the
   * picture format does not support transparency, then the server shall respond with a
   * non-transparent image (in other words, it is not an error for the client to always request
   * transparent maps regardless of format). When TRANSPARENT is set to FALSE, non-data pixels shall
   * be set to the value of BGCOLOR.
   * 
   * <p>
   * When the Layer has been declared opaque, then significant portions, or the entirety, of the
   * map may not be able to made transparent. Clients may still request TRANSPARENT=true. When the
   * FORMAT parameter contains a Graphic Element format, the TRANSPARENT parameter may be included
   * in the request but its value shall be ignored by the WMS.
   */
  public MapRequest setTransparent(boolean transparent) {
    this.transparent = Boolean.toString(transparent).toUpperCase();
    return this;
  }

  /**
   * Some geographic information may be available at multiple times (for example, an hourly weather
   * map). A WMS may announce available times in its service metadata, and the GetMap operation
   * includes a parameter for requesting a particular time. Depending on the context, time values
   * may appear as a single value, a list of values, or an interval. When providing temporal
   * information, a server should declare a default value in service metadata, and a server shall
   * respond with the default value if one has been declared and the client request does not include
   * a value.
   */
  public MapRequest setTime(Dimension time) {
    this.time = time.toString();
    return this;
  }

  /**
   * Some geographic information may be available at multiple elevations (for example, ozone
   * concentrations at different heights in the atmosphere). A WMS may announce available elevations
   * in its service metadata, and the GetMap operation includes an optional parameter for requesting
   * a particular elevation. A single elevation or depth value is a number whose units, and the
   * direction in which ordinates increment, are declared through a one- dimensional vertical CRS.
   * Depending on the context, elevation values may appear as a single value, a list of values, or
   * an interval.
   * 
   * <p>
   * A server may declare at most one vertical CRS for each layer. For the purposes of this
   * International Standard, the horizontal and vertical CRSs are treated as independent metadata
   * elements and request parameters.
   * 
   * <p>
   * A request for a map at a specific elevation includes an elevation value but does not include
   * the vertical CRS identifier (the horizontal CRS, which is included along with the horizontal
   * bounding box in the request parameters). When providing elevation information, a server should
   * declare a default value in service metadata, and a server shall respond with the default value
   * if one has been declared and the client request does not include a value.
   * 
   * <p>
   * Two types of Vertical CRS identifiers are permitted: label and URL identifiers:
   * 
   * <p>
   * <ul>
   * <li>Label: The identifier includes a namespace prefix, a colon, and a numeric or string code.
   * B.6 defines an optional vertical CRS labelled CRS:88 based on the North American Vertical
   * Datum 1988. If the namespace prefix is EPSG, then the vertical CRS is one of those defined in
   * the European Petroleum Survey Group database.
   * <li>URL: The identifier is a fully-qualified Uniform Resource Locator that references a
   * publicly-accessible file containing a definition of the CRS that is compliant with ISO 19111.
   * </ul>
   * If the height is the vertical component of a 3-dimensional CRS, the Vertical CRS identifier
   * shall be that of the 3- dimensional CRS.
   */
  public MapRequest setElevation(Dimension elevation) {
    this.elevation = elevation.toString();
    return this;
  }

  /**
   * Some geographic information may be available at other dimensions (for example, satellite images
   * in different wavelength bands). The dimensions other than the four space-time dimensions are
   * referred to as sample dimensions. A WMS may announce available sample dimensions in its
   * service metadata, and the GetMap operation includes a mechanism for requesting dimensional
   * values. Each sample dimension has a Name and one or more valid values.
   */
  public MapRequest addDimension(String name, Dimension value) {
    this.dimensions.put(name, value.toString());
    return this;
  }

  @Override
  public MediaType getMediaType() {
    return MediaType.parseMediaType(format);
  }

  /**
   * The extent string declares what value(s) along the Dimension axis are appropriate for the
   * corresponding layer.
   */
  public static class Dimension {

    private final String value;

    /**
     * A single value.
     */
    public Dimension(String value) {
      this.value = value;
    }

    /**
     * An interval defined by its lower and upper bounds and its resolution.
     * 
     * <p>
     * A resolution value of zero (as in min/max/0) means that the data are effectively at
     * infinitely-fine resolution for the purposes of making requests on the server. For instance,
     * an instrument which continuously monitors randomly- occurring data may have no explicitly
     * defined temporal resolution.
     */
    public Dimension(String min, String max, String resolution) {
      this.value = Joiner.on("/").join(Arrays.asList(min, max, resolution));
    }

    /**
     * A list of multiple values.
     */
    public Dimension(List<String> values) {
      this.value = Joiner.on(",").join(values);
    }

    /**
     * A list of multiple intervals.
     */
    public Dimension(Dimension[] dimensions) {
      this.value = Joiner.on(",").join(Arrays.asList(dimensions));
    }

    @Override
    public String toString() {
      return value;
    }

  }

}

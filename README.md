# Web Map Service Java client library #

[![build status](https://gitlab.com/openstreetcraft/wms/badges/master/pipeline.svg)](https://openstreetcraft.gitlab.io/wms/reports/integrationTest)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/de.ixilon/wms/badge.svg)](https://maven-badges.herokuapp.com/maven-central/de.ixilon/wms)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/e7c6c778888f4ad78595097dab5f28eb)](https://www.codacy.com/gl/openstreetcraft/wms/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=openstreetcraft/wms&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://app.codacy.com/project/badge/Coverage/e7c6c778888f4ad78595097dab5f28eb)](https://openstreetcraft.gitlab.io/wms/reports/jacoco/test/html/)
[![Javadoc Badge](https://img.shields.io/badge/javadoc-brightgreen.svg)](https://openstreetcraft.gitlab.io/wms/docs/javadoc/)

The OpenGIS Web Map Service Interface Standard (WMS) provides a simple HTTP
interface for requesting geo-registered map images from one or more distributed
geospatial databases. A WMS request defines the geographic layers and area of
interest to be processed. The response to the request is one or more
geo-registered map images (returned as JPEG, PNG, etc) that can be displayed in
a browser application. The interface also supports the ability to specify
whether the returned images should be transparent so that layers from multiple
servers can be combined or not.

More information may be found at http://www.opengeospatial.org/standards/wms

## Usage ##

Create an instance of [WebMapService](https://openstreetcraft.gitlab.io/wms/docs/javadoc/de/ixilon/wms/WebMapService.html).
See [integration test](https://gitlab.com/ixilon/wms/blob/master/src/integration-test/java/de/ixilon/wms/WebMapServiceIntegrationTest.java) for examples.

## Integrationtest ##

Integration testing requires a running GeoServer:

```
docker run --rm -p 8080:8080 winsent/geoserver
```

Add the following line to your `/etc/hosts`:

```
127.0.0.1 winsent-geoserver
```

Run the integration test:

```
./gradlew integrationTest
```

## Links ##

* [OpenGIS WMS Standard](http://www.opengeospatial.org/standards/wms)
* [Official GeoServer repository](https://github.com/geoserver/geoserver)
* [GeoServer Docker container](https://hub.docker.com/r/winsent/geoserver)
